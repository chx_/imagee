<?php

namespace Drupal\imagee;

use Drupal\Core\Image\Image as BaseImage;

/**
 * Image value object with support for external changes to the source file.
 */
class Image extends BaseImage {

  /**
   * @var callable
   */
  protected $toolkitFactory;

  /**
   * Image constructor.
   *
   * @param $source
   *   Path to the image source.
   * @param callable $toolkitFactory
   *   A callable returning an \Drupal\Core\Image\ImageToolkitInterface object.
   */
  public function __construct($source, callable $toolkitFactory = NULL) {
    if ($toolkitFactory) {
      $this->toolkitFactory = $toolkitFactory;
    }
    parent::__construct(($this->toolkitFactory)(), $source);
  }

  /**
   * @param $source
   *   Path to the new image source.
   *
   * @return $this
   */
  public function setSource($source) {
    // ImageToolkitBase::setSource() throws an exception on a second call, so
    // create a new toolkit.
    $this->__construct($source);
    return $this;
  }

}
