## When do I need this module?

If you are writing a Drupal 8 image effect plugin not using image toolkits,
for example by calling an external API. If you are writing a new image toolkit
for API integration, this module is not needed. End users only need this
module as a dependency. There is no UI, there's nothing to configure.

## Why is the module needed at all?

`\Drupal\Core\Image\Image` stores the size of the image and a toolkit object. 
The toolkit object holds the state of the image and no mechanism is provided
to refresh this based on an external change. Similarly, the `Image` object
has no mechanism to re-read the new image size. And the
`ImageEffectInterface::applyEffect` method mutates the passed in `Image` object
and returns a boolean, so there's no way to provide a new `Image`.

## So what is this module doing?

It extends the core `Image` object with a `setSource()` method. This is made
possible because these objects are created using the `image.factory` service,
but decorating this service is unusually tricky because of the lack of an
interface.

## Shouldn't this be core functionality?

It absolutely should be. Also, an `ImageFactoryInterface` needs to be added
and `applyEffect` should return an Image object and use exceptions for error
handling. The return value is not even used by the single caller of this
method. Either adding the `setSource` method as implemented here or changing
`applyEffect` to implement a pipeline would make this module unnecessary.
