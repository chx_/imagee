<?php

namespace Drupal\imagee;

use Drupal\Core\ImageToolkit\ImageToolkitManager;

use Drupal\Core\Image\ImageFactory as BaseImageFactory;

/**
 * Image factory with support for external changes to the source file.
 *
 * As there is no interface, several places directly hint with ImageFactory, so
 * it must be extended. However, this might get fixed in the future so for now
 * this class does not typehint the original image factory at all. This class
 * overrides every method to call the decorated image factory (which is almost
 * surely the core image factory). Only the get() method is changed very subtly:
 * it returns an \Drupal\imagee\Image object instead of an
 * \Drupal\Core\Image\Image object because the latter offers no way to pick
 * up external changes to the image source.
 */
class ImageFactory extends BaseImageFactory {

  /**
   * The image toolkit plugin manager.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitManager
   */
  protected $toolkitManager;

  /**
   * The image toolkit ID to use for this factory.
   *
   * @var string
   */
  protected $toolkitId;

  /**
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $originalImageFactory;

  /**
   * ImageFactory constructor.
   *
   * @param $originalImageFactory
   *   The original image factory. See class doxygen for the lack of typehint.
   * @param \Drupal\Core\ImageToolkit\ImageToolkitManager $imageToolkitManager
   */
  public function __construct($originalImageFactory, ImageToolkitManager $imageToolkitManager) {
    $this->originalImageFactory = $originalImageFactory;
    parent::__construct($imageToolkitManager);
  }

  public function setToolkitId($toolkit_id) {
    return $this->originalImageFactory->setToolkitId($toolkit_id);
  }

  public function getToolkitId() {
    return $this->originalImageFactory->getToolkitId();
  }

  public function getSupportedExtensions($toolkit_id = NULL) {
    return $this->originalImageFactory->getSupportedExtensions($toolkit_id);
  }

  /**
   * {@inheritdoc}
   */
  public function get($source = NULL, $toolkitId = NULL) {
    $toolkitId = $toolkitId ?: $this->toolkitId;
    $toolkitFactory = function () use ($toolkitId) {
      return $this->toolkitManager->createInstance($toolkitId);
    };
    return new Image($source, $toolkitFactory);
  }

}
